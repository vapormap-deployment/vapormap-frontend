FROM nginx:1.21-alpine as FINAL
WORKDIR /usr/share/nginx/html
COPY ./src/ .
CMD nginx -g "daemon off;"