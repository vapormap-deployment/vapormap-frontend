
# COMMON

build-gateway:
	docker build -t vapormap-gateway -f gateway/gateway.Dockerfile .

# PROD Commands

build-prod-fe:
	docker build -t vapormap-prod-fe -f prod-dep/prod-fe.Dockerfile .

deploy-prod: build-prod-images
	docker-compose  -p vapormap-prod -f prod-dep/prod.docker-compose.yml up
